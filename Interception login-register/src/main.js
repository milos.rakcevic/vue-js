import Vue from 'vue'
import App from './App.vue'
import Api from './components/Api'
import VueRouter from 'vue-router'
import Login from './components/Login.vue'
import Welcome from  './components/Welcome.vue'
import Signup from './components/Signup.vue'

Vue.use(VueRouter);

const routes = [
  { path:"/", component: Login},
  { path:"/welcome" , component: Welcome },
  { path:"/signup", component: Signup },
];

const router = new VueRouter({
  routes
});

Vue.config.productionTip = false

Vue.prototype.$http = Api;

Api.interceptors.request.use(
  config => {
    const token = localStorage.getItem("token");
    if (token) {
      config.headers.common["token"] = token;
    }
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

Api.interceptors.response.use(
  response => {
    if (response.status === 200 || response.status === 201) {
      router.replace({
        path: "/welcome"
      });
      return Promise.resolve(response);
      
    } else {
      return Promise.reject(response);
    }
  },
  error => {
      if (error.response.status) {
        switch (error.response.status) {
          case 400:
            alert("wrong username or password");
           
            break;
        
          case 401:
            // nema token
            alert("session expired or you dont have a token");
            break;
          case 403:
            // kad nije auth.
            router.replace({
              path: "/login",
              query: { redirect: router.currentRoute.fullPath }
            });
             break;
          case 404:
            alert('page not exist');
            break;
          case 502:
           setTimeout(() => {
             // obavjestenje - problem sa serverom
              router.replace({
                path: "/login",
                query: {
                  redirect: router.currentRoute.fullPath
                }
              });
            }, 1000);
        }
        return Promise.reject(error.response);
      }
    }
  );



new Vue({
  el: '#app',
  router,
  render: h => h(App),
}) 
